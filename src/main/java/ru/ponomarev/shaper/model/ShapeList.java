package ru.ponomarev.shaper.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * List of shapes is implemented as public singleton
 * It allows it to be accessible from controllers of any new application's view.
 */
public enum ShapeList {
    INSTANCE;

    // Model for shape table in main window.
    private ObservableList<Shape> shapeList = FXCollections.observableArrayList();

    {
        shapeList.add(Shape.createOval("oval1", 1, 3, 6, 7));
        shapeList.add(Shape.createCircle("oval2", 5, 8, 3));
        shapeList.add(Shape.createRectangle("rect1", 7, 2, 34, 5));
        shapeList.add(Shape.createRectangle("rect2", 17, 23, 4, 4));
        shapeList.add(Shape.createCircle("oval3", 100, 32, 22));
    }

    public ObservableList<Shape> getShapeList() {
        return shapeList;
    }
}
