package ru.ponomarev.shaper.model;

import javafx.util.converter.BooleanStringConverter;
import org.apache.commons.lang.builder.ToStringBuilder;
import ru.ponomarev.shaper.util.property.BooleanShapeProperty;
import ru.ponomarev.shaper.util.property.IntegerShapeProperty;
import ru.ponomarev.shaper.util.validator.ValidationRule;

/**
 * Oval is JavaFX bean with observable properties for syncronization with view.
 */
class Oval extends Shape {

    private final BooleanShapeProperty isCircle = new BooleanShapeProperty("isCircle", ValidationRule.NO_VALIDATION);

    private final IntegerShapeProperty radiusX = new IntegerShapeProperty("RadiusX", ValidationRule.POSITIVE_NUMBER);

    private final IntegerShapeProperty radiusY = new IntegerShapeProperty("RadiusY", ValidationRule.POSITIVE_NUMBER, false);

    Oval(String name, int x, int y, int radiusX, int radiusY) {
        super(name, x, y);
        setIsCircle(radiusX == radiusY);
        setRadiusX(radiusX);
        setRadiusY(radiusY);

        this.radiusY.isVisibleProperty().bind(isCircleProperty().not());
        isCircleProperty().addListener(observable -> {
            this.radiusY.setValue(this.radiusX.getValue());
        });
        this.radiusX.labelTextProperty().bindBidirectional(isCircleProperty(), new BooleanStringConverter() {
            @Override
            public String toString(Boolean isCircle) {
                return isCircle ? "Radius" : "RadiusX";
            }
        });
    }

    Oval() {
        this("", 0, 0, 0, 0);
    }

    public boolean getIsCircle() {
        return isCircle.getValue();
    }

    public void setIsCircle(boolean value) {
        isCircle.setValue(value);
    }

    public BooleanShapeProperty isCircleProperty() {
        return isCircle;
    }

    public int getRadiusX() {
        return radiusX.intValue();
    }

    public void setRadiusX(int value) {
        radiusX.setValue(value);
    }

    public IntegerShapeProperty radiusXProperty() {
        return radiusX;
    }

    public int getRadiusY() {
        return radiusY.intValue();
    }

    public void setRadiusY(int value) {
        radiusY.setValue(value);
    }

    public IntegerShapeProperty radiusYProperty() {
        return radiusY;
    }

    /**
     * Override this method in new shapes!
     * It is used for syncronizing data with ShapeDialog.
     *
     * @param source source oval
     */
    public void receiveValuesFromOtherOne(Shape source) {
        if (!(source instanceof Oval))
            throw new IllegalArgumentException("Class of source Shape is not equal to current shape.");
        Oval oval = (Oval) source;
        super.receiveValuesFromOtherOne(oval);
        setIsCircle(oval.getIsCircle());
        setRadiusX(oval.getRadiusX());
        setRadiusY(oval.getRadiusY());
    }

    @Override
    public Oval clone() {
        Oval newShape = new Oval();
        newShape.receiveValuesFromOtherOne(this);
        return newShape;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", getType())
                .append("name", getName())
                .append("x", getX())
                .append("y", getY())
                .append("isCircle", getIsCircle())
                .append("radiusX", getRadiusX())
                .append("radiusY", getRadiusY())
                .toString();
    }
}
