package ru.ponomarev.shaper.model;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import org.apache.commons.lang.builder.ToStringBuilder;
import ru.ponomarev.shaper.util.property.IntegerShapeProperty;
import ru.ponomarev.shaper.util.property.StringShapeProperty;
import ru.ponomarev.shaper.util.validator.ValidationRule;

/**
 * Shape is JavaFX bean with observable properties for syncronization with view.
 */
public class Shape implements Cloneable {

    private ReadOnlyStringProperty type = new ReadOnlyStringWrapper(getClass().getSimpleName());

    private final StringShapeProperty name = new StringShapeProperty("Name", ValidationRule.REQUIRED_FIELD);

    private final IntegerShapeProperty x = new IntegerShapeProperty("X", ValidationRule.NUMBER);

    private final IntegerShapeProperty y = new IntegerShapeProperty("Y", ValidationRule.NUMBER);

    Shape() {
        this("Shape", 0, 0);

    }

    Shape(String name, int x, int y) {
        setName(name);
        setX(x);
        setY(y);
    }


    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public StringShapeProperty nameProperty() {
        return name;
    }

    public int getX() {
        return x.get();
    }

    public void setX(int x) {
        this.x.setValue(x);
    }

    public IntegerShapeProperty xProperty() {
        return x;
    }

    public int getY() {
        return y.get();
    }

    public void setY(int y) {
        this.y.setValue(y);
    }

    public IntegerShapeProperty yProperty() {
        return y;
    }

    public String getType() {
        return type.getValue();
    }

    public ReadOnlyStringProperty shapeTypeProperty() {
        return type;
    }

    /**
     * Override this method in new shapes!
     * It is used for syncronizing data with ShapeDialog.
     *
     * @param source source
     */
    public void receiveValuesFromOtherOne(Shape source) {
        setName(source.getName());
        setX(source.getX());
        setY(source.getY());
    }

    @Override
    public Shape clone() {
        Shape newShape = new Shape();
        newShape.receiveValuesFromOtherOne(this);
        return newShape;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", getType())
                .append("name", getName())
                .append("x", getX())
                .append("y", getY())
                .toString();
    }

    /**
     * Text representation of type of current shape. If not defined, then "Shape"
     *
     * @return text representation of dialog's shape.
     */
    public static String shapeTypeString(Shape shape) {
        return shape == null ? "Shape" : shape.getType();
    }

    public static Shape createOval (String name, int x, int y, int radiusX, int radiusY){
        return new Oval (name, x, y, radiusX, radiusY);
    }

    public static Shape createCircle (String name, int x, int y, int radius){
        return new Oval (name, x, y, radius, radius);
    }

    public static Shape createRectangle (String name, int x, int y, int width, int height) {
        return new Rectangle (name, x, y, width, height);
    }

    public static Shape createSquare(String name, int x, int y, int width) {
        return new Rectangle (name, x, y, width, width);
    }

    public static Shape createDot (String name, int x, int y) {
        return new Shape (name, x, y);
    }

}
