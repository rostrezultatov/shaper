package ru.ponomarev.shaper.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import ru.ponomarev.shaper.util.property.IntegerShapeProperty;
import ru.ponomarev.shaper.util.validator.ValidationRule;

/**
 * Rectangle is JavaFX bean with observable properties for syncronization with view.
 */
public class Rectangle extends Shape {

    Rectangle(String name, int x, int y, int width, int height) {
        super(name, x, y);
        setWidth(width);
        setHeight(height);
    }

    Rectangle() {
        this("", 0, 0, 0, 0);
    }

    private final IntegerShapeProperty width = new IntegerShapeProperty("Width", ValidationRule.POSITIVE_NUMBER);

    private final IntegerShapeProperty height = new IntegerShapeProperty("Height", ValidationRule.POSITIVE_NUMBER);

    public int getWidth() {
        return width.intValue();
    }

    public void setWidth(int value) {
        width.setValue(value);
    }

    public IntegerShapeProperty widthProperty() {
        return width;
    }

    public int getHeight() {
        return height.intValue();
    }

    public void setHeight(int value) {
        height.setValue(value);
    }

    public IntegerShapeProperty heightProperty() {
        return height;
    }

    /**
     * Override this method in new shapes!
     * It is used for syncronizing data with ShapeDialog.
     *
     * @param source source oval
     */
    public void receiveValuesFromOtherOne(Shape source) {
        if (!(source instanceof Rectangle))
            throw new IllegalArgumentException("Class of source Shape is not equal to current shape.");
        Rectangle rectangle = (Rectangle) source;
        super.receiveValuesFromOtherOne(rectangle);
        setWidth(rectangle.getWidth());
        setHeight(rectangle.getHeight());
    }

    @Override
    public Rectangle clone() {
        Rectangle newShape = new Rectangle();
        newShape.receiveValuesFromOtherOne(this);
        return newShape;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", getType())
                .append("name", getName())
                .append("x", getX())
                .append("y", getY())
                .append("width", getWidth())
                .append("height", getHeight())
                .toString();
    }
};
