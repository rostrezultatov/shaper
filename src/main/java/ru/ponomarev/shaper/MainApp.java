package ru.ponomarev.shaper;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ponomarev.shaper.controller.MainController;

public class MainApp extends Application {

    private static final Logger log = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    public void start(Stage stage) throws Exception {

        log.info("Starting Shaper JavaFX application");

        Platform.runLater(() -> {
            try {
                String fxmlFile = "/fxml/main.fxml";
                log.debug("Loading FXML for main view from: {}", fxmlFile);
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(Class.class.getResource(fxmlFile));
                Parent rootNode = loader.load(getClass().getResourceAsStream(fxmlFile));

                ((MainController) loader.getController()).setMainStage(stage);

                log.debug("Showing JFX scene");
                Scene scene = new Scene(rootNode, 400, 250);
                scene.getStylesheets().add("/styles/styles.css");

                stage.setTitle("Shaper Application");
                stage.setScene(scene);
                stage.show();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }
}
