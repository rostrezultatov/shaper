package ru.ponomarev.shaper.controller;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ponomarev.shaper.MainApp;
import ru.ponomarev.shaper.model.Shape;
import ru.ponomarev.shaper.util.ShapeUtils;
import ru.ponomarev.shaper.util.property.ShapeProperty;
import ru.ponomarev.shaper.util.validator.TextFieldValidator;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ShapeDialogController {

    @FXML
    private GridPane propertyGrid;

    @FXML
    private Button saveButton;

    @FXML
    private Label titleLabel;

    @FXML
    private Label errorLabel;

    private Stage dialogStage;

    private Shape originalShape;

    private Shape tempShape;

    private boolean saveClicked = false;

    private static final Logger log = LoggerFactory.getLogger(ShapeDialogController.class);

    private TextFieldValidator validator;

    /**
     * Here are all {@link javafx.scene.control.Control}'s with their names
     */
    private Map<Control, Pair<ShapeProperty, Label>> shapeInputs;

    {
        validator = TextFieldValidator.INSTANCE;
        shapeInputs = new LinkedHashMap<>();

        validator.setOnErrorAction((textField, message) -> {
            errorLabel.setText(validator.lastErrorMessage());
            errorLabel.setVisible(true);
            textField.getStyleClass().add("validation-error");
            saveButton.setDisable(true);
        });

        validator.setOnErrorCorrectedAction(textField -> {
            errorLabel.setText(validator.lastErrorMessage());
            textField.getStyleClass().removeAll("validation-error");
            if (!validator.isAnyValidationError()) {
                saveButton.setDisable(false);
            }
        });
    }

    /**
     * Tells to dialog creator if user wants to save edited data.
     *
     * @return true if user clicked "save" button.
     */
    boolean isSaveClicked() {
        return saveClicked;
    }

    /**
     * Creates temporary copy of given shape and works with it.
     * If user preses "save" button, it's properties transfer to original shape.
     *
     * @param shape - original shape.
     */
    void setShape(Shape shape) {
        this.originalShape = shape;
        this.tempShape = originalShape.clone();
        titleLabel.setText(Shape.shapeTypeString(shape) + "'s properties");
        createShapePropertyControls();
    }

    void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Fills {@link ShapeDialogController#shapeInputs} by tempShape's properties.
     * Invokes once, when Dialog created.
     */
    private void createShapePropertyControls() {
        // find all properties and theirs names from tempShape by reflection.
        List<ShapeProperty> shapeProperties = ShapeUtils.retrieveShapeProperties(tempShape);
        shapeProperties.forEach(prop -> {
            String propLabel = prop.labelTextProperty().getValue();
            // create control and label for every property
            Control propInput = ShapeUtils.createInputControlProperty(prop);
            propInput.setId(propLabel);
            Label propNameLabel = new Label(propLabel);
            propNameLabel.textProperty().bindBidirectional(prop.labelTextProperty());
            // make bindings
            ShapeUtils.bindControlWithShapeProperty(propInput, prop);
            propNameLabel.visibleProperty().bindBidirectional(propInput.visibleProperty());
            propInput.visibleProperty().addListener(this::checkPropertyShouldValidate); // if visibility changed then change error message
            propNameLabel.textProperty().addListener(observable -> checkControlShouldValidate(propInput));  // if label changed then change error message
            //propInput.visibleProperty().addListener(changedVisible -> invalidateVisibleInputs()); // bug with "disappearing" visible buttons
            // fill collection with tempShape's UI data
            shapeInputs.put(propInput, new Pair<>(prop, propNameLabel));
        });

        //draw created inputs
        invalidateVisibleInputs();
    }

    /**
     * Draw tempShape's UI controls on Dialog.
     * Invokes every time when any property's visibility is changed.
     */
    private void invalidateVisibleInputs() {
        final int[] i = {0};
        log.debug("INVOKED invalidateVisibleInputs()");
        propertyGrid.getChildren().clear();
        shapeInputs.forEach((propInput, namedProperty) -> {
            ShapeProperty property = namedProperty.getKey();
            Label propertyLabel = namedProperty.getValue();
            //if (property.isVisibleProperty().getValue()) { // bug with "disappearing" visible buttons
            // new row in GridPane
            addRowConstraint();
            // add label
            propertyGrid.add(propertyLabel, 0, i[0]);
            GridPane.setHalignment(namedProperty.getValue(), HPos.RIGHT);
            // add control
            propertyGrid.add(propInput, 1, i[0]);
            i[0]++;
            checkControlShouldValidate(propInput);
        });
    }

    /**
     * Adds control to validator if it is {@link TextField} and linked to any {@link ShapeProperty}
     *
     * @param control - control that will be checking
     */
    private void addControlForValidation(Control control) {
        if (shapeInputs.containsKey(control) && control instanceof TextField) {
            ShapeProperty property = shapeInputs.get(control).getKey();
            validator.addValidationRule((TextField) control, property.getValidationRule(), property.getErrorMessage());
        }
    }

    /**
     * This method removes {@link Control} from validation if there is it.
     *
     * @param control to remove from validator
     */
    private void removeControlFromValidator(Control control) {
        if (shapeInputs.containsKey(control) && control instanceof TextField) {
            validator.removeValidationRule((TextField) control);
        }
    }

    /**
     * Updates validation status of {@link javafx.beans.Observable}
     *
     * @param property property that will be checking
     */
    private void checkPropertyShouldValidate(Observable property) {
        if (!(property instanceof ReadOnlyProperty))
            return;
        if (!(((ReadOnlyProperty) property).getBean() instanceof TextField))
            return;
        TextField input = (TextField) ((ReadOnlyProperty) property).getBean();
        checkControlShouldValidate(input);
    }

    private void checkControlShouldValidate(Control input) {
        if (input.isVisible())
            addControlForValidation(input);
        else
            removeControlFromValidator(input);
        if (input instanceof TextField)
            validator.validateTextFieldInput((TextField) input);
    }

    /**
     * Add one row to {@link GridPane}
     */
    private void addRowConstraint() {
        propertyGrid.getRowConstraints().add(new RowConstraints(10, 30, 100, Priority.SOMETIMES, VPos.CENTER, true));
    }

    /**
     * Saves shape, edited by user and clears temp shape.
     *
     * @param actionEvent
     */
    @FXML
    private void onSave(ActionEvent actionEvent) {
        saveClicked = true;
        originalShape.receiveValuesFromOtherOne(tempShape);
        tempShape = null;
        dialogStage.close();
    }

    /**
     * On "cansel" button - close dialog.
     *
     * @param actionEvent
     */
    @FXML
    private void onCancel(ActionEvent actionEvent) {
        dialogStage.close();
    }

    /**
     * Creates ShapeDialog and waits for it closing.
     *
     * @param shape - edited shape
     * @return - true if user closed dialog by clicking "save" button.
     */
    static boolean showShapePropertiesDialog(Shape shape, Stage mainStage) {
        if (!(Platform.isFxApplicationThread()))
            throw new IllegalThreadStateException("Creating new Dialog not in UI thread.");

        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/fxml/ShapeDialog.fxml"));
            GridPane pane = loader.load(MainApp.class.getResourceAsStream("/fxml/ShapeDialog.fxml"));
            pane.getStylesheets().add("/styles/styles.css");

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Shape");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainStage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Set the shape into the dialog's controller.
            ShapeDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setShape(shape);
            log.debug("new shape properties dialog initiated by shape: " + shape);
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isSaveClicked();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
