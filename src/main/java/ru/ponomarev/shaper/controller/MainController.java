package ru.ponomarev.shaper.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ponomarev.shaper.model.Shape;

/**
 * This controller is between main table (view) and collection of shapes (model).
 */
public class MainController {

    private final static Logger log = LoggerFactory.getLogger(MainController.class);

    /**
     * Automatically created instance of nested views's controller
     */
    @FXML
    private ShapeTableController shapeTablePaneController;

    // Reference to the main application.
    private Stage mainStage;

    @FXML
    private void closeApp(ActionEvent event) {
        log.debug("Close application");
        if (mainStage != null) mainStage.close();
    }

    @FXML
    private void createRectangle(ActionEvent event) {
        log.debug("Create Rectangle");
        Shape shape = Shape.createRectangle("", 0, 0, 0, 0);
        showEditShapeDialog(shape);
    }

    private void showEditShapeDialog(Shape shape) {
        boolean saveClicked = ShapeDialogController.showShapePropertiesDialog(shape, mainStage);
        if (saveClicked) {
            shapeTablePaneController.addShape(shape);
        }
    }

    @FXML
    private void createOval(ActionEvent event) {
        log.debug("Create Oval");
        Shape tempOval = Shape.createCircle("", 0, 0, 0);
        showEditShapeDialog(tempOval);
    }

    @FXML
    private void removeShapes(ActionEvent event) {
        log.debug("Remove shapes. Event = " + event);
        shapeTablePaneController.removeSelectedShapes();
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

}