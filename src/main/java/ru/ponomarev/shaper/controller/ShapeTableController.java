package ru.ponomarev.shaper.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ponomarev.shaper.model.Shape;
import ru.ponomarev.shaper.model.ShapeList;

public class ShapeTableController {

    private static final Logger log = LoggerFactory.getLogger(ShapeTableController.class);

    @FXML
    private TableView<Shape> shapeTable;

    @FXML
    private TableColumn<Shape, Integer> xColumn;

    @FXML
    private TableColumn<Shape, Integer> yColumn;

    @FXML
    private TableColumn<Shape, String> nameColumn;

    @FXML
    private TableColumn<Shape, String> typeColumn;

    // Reference to the main application.
    private Stage mainStage;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // using model.ShapeList
        shapeTable.setItems(ShapeList.INSTANCE.getShapeList());
        // Initialize the table.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        typeColumn.setCellValueFactory(cellData -> cellData.getValue().shapeTypeProperty());
        xColumn.setCellValueFactory(cellData -> cellData.getValue().xProperty().asObject());
        yColumn.setCellValueFactory(cellData -> cellData.getValue().yProperty().asObject());
        // Ability to select several columns
        shapeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // Automatic column width: 40%, 30%, 15%, 15%
        nameColumn.prefWidthProperty().bind(shapeTable.widthProperty().multiply(0.40));
        typeColumn.prefWidthProperty().bind(shapeTable.widthProperty().multiply(0.30));
        xColumn.prefWidthProperty().bind(shapeTable.widthProperty().multiply(0.15));
        yColumn.prefWidthProperty().bind(shapeTable.widthProperty().multiply(0.15));
    }

    void removeSelectedShapes() {
        ObservableList<Shape> selectedShapes = shapeTable.getSelectionModel().getSelectedItems();
        shapeTable.getItems().removeAll(selectedShapes);
    }

    void addShape(Shape shape) {
        shapeTable.getItems().add(shape);
    }

    /**
     * Handles doubleClick on table row.
     *
     * @param event click event
     */
    @FXML
    private void onShapeTableMousePressed(MouseEvent event) {
        if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
            ShapeDialogController.showShapePropertiesDialog(shapeTable.getSelectionModel().getSelectedItem(), mainStage);
        }
    }
}
