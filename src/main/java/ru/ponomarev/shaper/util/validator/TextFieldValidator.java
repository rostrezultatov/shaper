package ru.ponomarev.shaper.util.validator;

import javafx.beans.InvalidationListener;
import javafx.beans.property.StringProperty;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.util.Pair;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Validator is singleton object used for checking users' inputs in TextFields.
 * To add input validation, use
 * {@link TextFieldValidator#addValidationRule(TextField, ValidationRule, String)}
 * To set custom behaviour on validation error, use
 * {@link TextFieldValidator#setOnErrorAction} and {@link TextFieldValidator#setOnErrorCorrectedAction(Consumer)}
 */
public enum TextFieldValidator {

    /**
     * {@link TextFieldValidator} singleton is implemented as enum with one element.
     */
    INSTANCE;

    private static final Logger log = LoggerFactory.getLogger(TextFieldValidator.class);

    /**
     * Collection of validation rules for every user's {@link TextField} input
     */
    private final Map<TextField, Pair<ValidationRule, String>> validationRules;

    /**
     * This action is called when validation fails.
     */
    private BiConsumer<TextField, String> onErrorAction;

    /**
     * This action is called when error is corrected.
     */
    private Consumer<TextField> onErrorCorrectedAction;

    /**
     * This set is for determining if this moment is first time after error corretion.
     */
    private Set<TextField> errorInputs;

    public boolean isAnyValidationError() {
        return errorInputs.size() != 0;
    }

    /**
     * Logging validation errors
     *
     * @param textField    user input
     * @param errorMessage message printed if validation fails
     */
    private void logValidationError(TextField textField, String errorMessage) {
        log.debug(String.format("Validation failed. Entered: %1$s. Validation error: %2$s TF styleclasses are %3$s", textField.getText(), errorMessage, textField.getStyleClass()));
    }

    /**
     * Logging error corrections.
     *
     * @param textField user input
     */
    private void logValidationErrorCorrected(TextField textField) {
        log.debug("Error of validation corrected. Entered: " + textField.getText());
    }

    {
        validationRules = new LinkedHashMap<>();
        errorInputs = new HashSet<>();
        onErrorAction = this::logValidationError;
        onErrorCorrectedAction = this::logValidationErrorCorrected;
    }

    /**
     * This method is handler for {@link TextField#setOnKeyReleased(EventHandler)}
     *
     * @param event contains all info about user's keyboard action
     */

    private void handleUserInput(KeyEvent event) {
        validateTextFieldInput((TextField) event.getSource());
    }

    /**
     * This method is {@link InvalidationListener}'s implementation,
     * applyed to StringProperty of TextField.
     *
     * @param stringProperty text property of TextField
     */
    private void handlePropertyChange(javafx.beans.Observable stringProperty) {
        Validate.isTrue(stringProperty instanceof StringProperty);
        Validate.isTrue(((StringProperty) stringProperty).getBean() instanceof TextField);

        TextField input = (TextField) ((StringProperty) stringProperty).getBean();
        validateTextFieldInput(input);
    }

    /**
     * Invoke this method when {@link TextField} changed.
     * If there is validation error, calls {@link TextFieldValidator#onErrorAction}
     * If there are no errors, calls {@link TextFieldValidator#onErrorCorrectedAction}
     * first time after error correction
     *
     * @param input changed input
     */
    public void validateTextFieldInput(TextField input) {
        ValidationRule condition = getValidationCondition(input);
        String errorMessage = getErrorMessage(input);

        if (!condition.test(input.getText())) {
            errorInputs.add(input);
            onErrorAction.accept(input, errorMessage);
        } else if (errorInputs.contains(input)) {
            errorInputs.remove(input);
            onErrorCorrectedAction.accept(input);
        }
    }

    /**
     * When there are several validation errors, this method allows
     * to display message of input that is first inserted to {@link TextFieldValidator#validationRules}
     *
     * @return error message of "highest" user input.
     */
    public String lastErrorMessage() {
        TextField firstErrorInput = null;
        for (TextField input : validationRules.keySet()) {
            if (errorInputs.contains(input)) {
                firstErrorInput = input;
                break;
            }
        }
        return getErrorMessage(firstErrorInput);
    }

    /**
     * Returns error message for given {@link TextField}.
     * It can be displayed when validation fails.
     *
     * @param input user's input. If input == null then returns empty string.
     * @return failed validation message.
     */
    private String getErrorMessage(TextField input) {
        return input == null || !validationRules.containsKey(input) ? "" : validationRules.get(input).getValue();
    }

    /**
     * Returns validation condition for given {@link TextField}
     *
     * @param input user's text input.
     * @return condition for validation
     */
    public ValidationRule getValidationCondition(TextField input) {
        return validationRules.containsKey(input) ? validationRules.get(input).getKey() : ValidationRule.NO_VALIDATION;
    }

    /**
     * Adding custom behaviour on validation errors {@link TextFieldValidator#logValidationError}
     *
     * @param errorAction new error behaviour
     */
    public void setOnErrorAction(BiConsumer<TextField, String> errorAction) {
        this.onErrorAction = ((BiConsumer<TextField, String>) this::logValidationError).andThen(errorAction);
    }

    /**
     * Revert to state before validation error {@link TextFieldValidator#logValidationError}
     *
     * @param errorCorrectedAction behavior, returning to normal state
     */
    public void setOnErrorCorrectedAction(Consumer<TextField> errorCorrectedAction) {
        this.onErrorCorrectedAction = ((Consumer<TextField>) this::logValidationErrorCorrected).andThen(errorCorrectedAction);
    }

    /**
     * This method registers new validation rules and applies it to new {@link TextField}
     *
     * @param input     textField for validation
     * @param condition validation type
     * @param message   error message handled by {@link TextFieldValidator#onErrorAction}
     */
    public void addValidationRule(TextField input, ValidationRule condition, String message) {
        validationRules.put(input, new Pair<>(condition, message));
        // input.setOnKeyReleased(this::handleUserInput); // it does not work in ui tests
        input.textProperty().addListener(this::handlePropertyChange);
        // check immediately
        handlePropertyChange(input.textProperty());
    }

    /**
     * This method removes {@link TextField} from validation
     *
     * @param input to remove from validator
     */
    public void removeValidationRule(TextField input) {
        validationRules.remove(input);
    }
}
