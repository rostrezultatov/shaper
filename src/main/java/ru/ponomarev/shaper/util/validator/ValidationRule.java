package ru.ponomarev.shaper.util.validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.util.function.Predicate;

/**
 * These rules validate String inputs in UI.
 * If rule triggered error, it returns error message for given user's input.
 */
public enum ValidationRule implements Predicate<String> {
    REQUIRED_FIELD {
        @Override
        public String createErrorMessage(String label) {
            return label + " is required field";
        }

        @Override
        public boolean test(String s) {
            return !StringUtils.isEmpty(s.trim());
        }
    },
    NUMBER {
        @Override
        public String createErrorMessage(String label) {
            return label + " must be a number";
        }

        @Override
        public boolean test(String s) {
            return NumberUtils.isNumber(s);
        }
    },
    POSITIVE_NUMBER {
        @Override
        public String createErrorMessage(String label) {
            return label + " must be a positive number";
        }

        @Override
        public boolean test(String s) {
            return NumberUtils.isNumber(s) && Integer.valueOf(s) > 0;
        }
    },
    NO_VALIDATION {
        @Override
        public String createErrorMessage(String label) {
            return "";
        }

        @Override
        public boolean test(String s) {
            return true;
        }
    };

    /**
     * message displayed in UI if user entered wrong value
     *
     * @return
     */
    public abstract String createErrorMessage(String propertyLabel);
}