package ru.ponomarev.shaper.util.property;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.StringConverter;
import javafx.util.converter.BooleanStringConverter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import ru.ponomarev.shaper.util.validator.ValidationRule;

public class BooleanShapeProperty extends SimpleBooleanProperty implements ShapeProperty<Boolean> {

    private final ValidationRule validationRule;

    private final BooleanProperty visibleProperty;

    private final StringProperty labelTextProperty;

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     *
     * @param labelText        - label text for this property in view.
     * @param defaultValue     - default value.
     * @param validationRule   - how to determine that entered value is valid
     * @param isVisibleDefault - is this property visible on ShapeDialog.
     */
    public BooleanShapeProperty(String labelText, boolean defaultValue, ValidationRule validationRule, boolean isVisibleDefault) {
        this.labelTextProperty = new SimpleStringProperty(labelText);
        this.setValue(defaultValue);
        this.validationRule = validationRule;
        this.visibleProperty = new SimpleBooleanProperty(isVisibleDefault);
    }

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * Default value is true.
     *
     * @param labelText        - label text for this property in view.
     * @param validationRule   - how to determine that entered value is valid
     * @param isVisibleDefault - is this property visible on ShapeDialog.
     */
    public BooleanShapeProperty(String labelText, ValidationRule validationRule, boolean isVisibleDefault) {
        this(labelText, true, validationRule, isVisibleDefault);
    }

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * It is visible on ShapeDialog by default.
     * Default value is true.
     *
     * @param labelText      - label text for this property in view.
     * @param defaultValue   - default value
     * @param validationRule - how to determine that entered value is valid
     */
    public BooleanShapeProperty(String labelText, boolean defaultValue, ValidationRule validationRule) {
        this(labelText, defaultValue, validationRule, true);
    }

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * It is visible on ShapeDialog by default.
     * Default value is true.
     *
     * @param labelText      - label text for this property in view.
     * @param validationRule - how to determine that entered value is valid
     */
    public BooleanShapeProperty(String labelText, ValidationRule validationRule) {
        this(labelText, validationRule, true);
    }

    /**
     * Returns bidirectional converter string-boolean,
     * it used for synchronizing shape dialog's input with temp shape.
     * This realization of converter hides all user's errors,
     * because we have {@link ru.ponomarev.shaper.util.validator.TextFieldValidator} for error handling.
     *
     * @return BooleanStringConverter hiding user's errors
     */
    @Override
    public StringConverter<Boolean> getStringConverter() {
        return new BooleanStringConverter() {
            @Override
            public Boolean fromString(String value) {
                Boolean result = super.fromString(value);
                return result != null && result;
            }
        };
    }

    @Override
    public ValidationRule getValidationRule() {
        return validationRule;
    }

    @Override
    public BooleanProperty isVisibleProperty() {
        return visibleProperty;
    }

    @Override
    public StringProperty labelTextProperty() {
        return labelTextProperty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BooleanShapeProperty that = (BooleanShapeProperty) o;

        return new EqualsBuilder()
                .append(getValue(), that.getValue())
                .append(validationRule, that.validationRule)
                .append(visibleProperty, that.visibleProperty)
                .append(labelTextProperty, that.labelTextProperty)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getValue())
                .append(validationRule)
                .append(visibleProperty)
                .append(labelTextProperty)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("labelTextProperty", labelTextProperty)
                .append("value", getValue())
                .append("validationRule", validationRule)
                .append("visibleProperty", visibleProperty)
                .toString();
    }
}

