package ru.ponomarev.shaper.util.property;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import ru.ponomarev.shaper.util.validator.ValidationRule;

public class StringShapeProperty extends SimpleStringProperty implements ShapeProperty<String> {

    private final ValidationRule validationRule;

    private final BooleanProperty visibleProperty;

    private final StringProperty labelTextProperty;

    /**
     * Constructor of {@link SimpleStringProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     *
     * @param labelText        - label text for this property in view.
     * @param defaultValue     - default value
     * @param validationRule   - how to determine that entered value is valid
     * @param isVisibleDefault - is this property visible on ShapeDialog.
     */
    public StringShapeProperty(String labelText, String defaultValue, ValidationRule validationRule, boolean isVisibleDefault) {
        this.labelTextProperty = new SimpleStringProperty(labelText);
        setValue(defaultValue);
        this.validationRule = validationRule;
        this.visibleProperty = new SimpleBooleanProperty(isVisibleDefault);
    }

    /**
     * Constructor of {@link SimpleStringProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * Default value is empty string.
     *
     * @param labelText        - label text for this property in view.
     * @param validationRule   - how to determine that entered value is valid
     * @param isVisibleDefault - is this property visible on ShapeDialog.
     */
    public StringShapeProperty(String labelText, ValidationRule validationRule, boolean isVisibleDefault) {
        this(labelText, "", validationRule, isVisibleDefault);
    }

    /**
     * Constructor of {@link SimpleStringProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * It is visible on ShapeDialog by default.
     *
     * @param labelText      - label text for this property in view.
     * @param defaultValue   - default value
     * @param validationRule - how to determine that entered value is valid
     */
    public StringShapeProperty(String labelText, String defaultValue, ValidationRule validationRule) {
        this(labelText, defaultValue, validationRule, true);
    }

    /**
     * Constructor of {@link SimpleStringProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * It is visible on ShapeDialog by default.
     * Default value is empty string.
     *
     * @param labelText      - label text for this property in view.
     * @param validationRule - how to determine that entered value is valid
     */
    public StringShapeProperty(String labelText, ValidationRule validationRule) {
        this(labelText, validationRule, true);
    }

    /**
     * Transparent converter from text user unput to String shape's property
     *
     * @return new DefaultStringConverter
     */
    @Override
    public StringConverter<String> getStringConverter() {
        return new DefaultStringConverter();
    }

    @Override
    public ValidationRule getValidationRule() {
        return validationRule;
    }

    @Override
    public BooleanProperty isVisibleProperty() {
        return visibleProperty;
    }

    @Override
    public StringProperty labelTextProperty() {
        return labelTextProperty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        StringShapeProperty that = (StringShapeProperty) o;

        return new EqualsBuilder()
                .append(getValue(), that.getValue())
                .append(validationRule, that.validationRule)
                .append(visibleProperty, that.visibleProperty)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getValue())
                .append(validationRule)
                .append(visibleProperty)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("value", getValue())
                .append("validationRule", validationRule)
                .append("visibleProperty", visibleProperty)
                .toString();
    }
}
