package ru.ponomarev.shaper.util.property;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.util.StringConverter;
import ru.ponomarev.shaper.util.validator.ValidationRule;

/**
 * Interface for accessing all neccessary data of shape's property for displaying property's input.
 */
public interface ShapeProperty<T> extends Property<T> {

    /**
     * Value of property. This method is already implemented in all classes like
     * {@link javafx.beans.property.SimpleStringProperty}, etc.
     *
     * @return value of property
     */
    T getValue();

    /**
     * Bidirectional converter of property value and string.
     *
     * @return proper StringConverter.
     */
    StringConverter<T> getStringConverter();

    /**
     * Rule for validating property in Dialog.
     *
     * @return validation rule.
     */
    ValidationRule getValidationRule();

    /**
     * Message that is shown when validation fails.
     *
     * @return error message
     */
    default String getErrorMessage() {
        return getValidationRule().createErrorMessage(labelTextProperty().getValue());
    }

    /**
     * Some properties are temporarily not visible in Dialog.
     * IsVisible property allows to setup UI and model behavior depending of this one.
     *
     * @return IsVisible property
     */
    BooleanProperty isVisibleProperty();

    /**
     * Defines label's text in view.
     *
     * @return labelTextProperty
     */
    StringProperty labelTextProperty();

}
