package ru.ponomarev.shaper.util.property;

import javafx.beans.property.*;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import ru.ponomarev.shaper.util.validator.ValidationRule;

public class IntegerShapeProperty extends SimpleIntegerProperty implements ShapeProperty<Number> {

    private final ValidationRule validationRule;

    private final BooleanProperty visibleProperty;

    private final StringProperty labelTextProperty;

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     *
     * @param labelText        - label text for this property in view.
     * @param defaultValue     - default value of property
     * @param validationRule   - how to determine that entered value is valid
     * @param isVisibleDefault - is this property visible on ShapeDialog.
     */
    public IntegerShapeProperty(String labelText, int defaultValue, ValidationRule validationRule, boolean isVisibleDefault) {
        this.labelTextProperty = new SimpleStringProperty(labelText);
        this.setValue(defaultValue);
        this.validationRule = validationRule;
        this.visibleProperty = new SimpleBooleanProperty(isVisibleDefault);
    }

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * By default value is 0.
     *
     * @param labelText        - label text for this property in view.
     * @param validationRule   - how to determine that entered value is valid
     * @param isVisibleDefault - is this property visible on ShapeDialog.
     */
    public IntegerShapeProperty(String labelText, ValidationRule validationRule, boolean isVisibleDefault) {
        this(labelText, 0, validationRule, isVisibleDefault);
    }

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * By default value is 0.
     *
     * @param labelText      - label text for this property in view.
     * @param defaultValue   - default value
     * @param validationRule - how to determine that entered value is valid
     */
    public IntegerShapeProperty(String labelText, int defaultValue, ValidationRule validationRule) {
        this(labelText, defaultValue, validationRule, true);
    }

    /**
     * Constructor of {@link IntegerShapeProperty}, extended by data for proper interacting in ShapeDialog
     * For accessing name of property for displaying, use {@link javafx.beans.property.ReadOnlyProperty}
     * It is visible on ShapeDialog by default.
     * By default value is 0.
     *
     * @param labelText      - label text for this property in view.
     * @param validationRule - how to determine that entered value is valid
     */
    public IntegerShapeProperty(String labelText, ValidationRule validationRule) {
        this(labelText, validationRule, true);
    }

    /**
     * Returns bidirectional converter string-integer,
     * it used for synchronizing shape dialog's input with temp shape.
     * This realization of converter hides all user's errors,
     * because we have {@link ru.ponomarev.shaper.util.validator.TextFieldValidator} for it.
     *
     * @return IntegerStringConverter hiding user's errors
     */
    @Override
    public StringConverter<Number> getStringConverter() {
        return new NumberStringConverter() {
            @Override
            public Number fromString(String value) {
                Number result;
                try {
                    result = super.fromString(value);
                    return result == null ? 0 : result;
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

    @Override
    public ValidationRule getValidationRule() {
        return validationRule;
    }

    @Override
    public BooleanProperty isVisibleProperty() {
        return visibleProperty;
    }

    @Override
    public StringProperty labelTextProperty() {
        return labelTextProperty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        IntegerShapeProperty that = (IntegerShapeProperty) o;

        return new EqualsBuilder()
                .append(getValue(), that.getValue())
                .append(validationRule, that.validationRule)
                .append(visibleProperty, that.visibleProperty)
                .append(labelTextProperty, that.labelTextProperty)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getValue())
                .append(validationRule)
                .append(visibleProperty)
                .append(labelTextProperty)
                .toHashCode();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("value", getValue())
                .append("validationRule", validationRule)
                .append("visibleProperty", visibleProperty)
                .append("labelTextProperty", labelTextProperty)
                .toString();
    }
}
