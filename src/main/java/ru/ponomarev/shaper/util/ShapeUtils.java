package ru.ponomarev.shaper.util;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import org.apache.commons.lang.Validate;
import ru.ponomarev.shaper.model.Shape;
import ru.ponomarev.shaper.util.property.BooleanShapeProperty;
import ru.ponomarev.shaper.util.property.ShapeProperty;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;


/**
 * Set of utility methods for working with {@link ru.ponomarev.shaper.model.Shape} objects.
 */
public interface ShapeUtils {

    /**
     * Retrieves from given class all {@link Field}s that are implementations of {@link ShapeProperty} interface.
     *
     * @param clazz Shape's class
     * @return list of fields
     */
    static List<Field> getUiPropertiesFromClass(Class<? extends Shape> clazz) {
        List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
        fields.forEach(field -> field.setAccessible(true));
        return fields.stream().filter(field -> ShapeProperty.class.isAssignableFrom(field.getType())).collect(Collectors.toList());
    }

    /**
     * Retrieves pair {@link ShapeProperty} and property's name from {@link Field} of concrete {@link Shape}
     *
     * @param propertyField field of shape's class
     * @param shape         shape instance for retrieving it's named {@link ShapeProperty}
     * @return named {@link ShapeProperty}
     */
    static ShapeProperty getPropertyFromShape(Field propertyField, Shape shape) {
        // check argument
        Validate.isTrue(ShapeProperty.class.isAssignableFrom(propertyField.getType()));
        // get name from annotation
        try {
            return (ShapeProperty) propertyField.get(shape);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retrieves list of all classes that are super classes of given one.
     *
     * @param klass class
     * @return class hierarchy
     */
    static List<Class> getClassHierarchy(Class klass) {
        List<Class> hierarchy = new LinkedList<>();
        Class currentClass = klass;
        do {
            hierarchy.add(currentClass);
            currentClass = currentClass.getSuperclass();
        } while (currentClass.getSuperclass() != null);
        return hierarchy;
    }

    /**
     * Collects all properties of shape - iterates over all superclasses
     * to gather properties in right order - as they are declared .java file.
     *
     * @param shape - shape for retrieving properties.
     * @return list of pairs: {@link ShapeProperty} and property's name
     */
    static List<ShapeProperty> retrieveShapeProperties(Shape shape) {
        List<ShapeProperty> namedProps = new LinkedList<>();
        List<Class> shapeHierarchy = getClassHierarchy(shape.getClass());
        // finds class hierarchy of given shape
        ListIterator<Class> li = shapeHierarchy.listIterator(shapeHierarchy.size());
        // from every class in hierarchy properties gather from top of hierarchy.
        while (li.hasPrevious()) {
            getUiPropertiesFromClass(li.previous()).forEach(field -> {
                namedProps.add(getPropertyFromShape((Field) field, shape));
            });
        }
        return namedProps;
    }

    /**
     * For boolean shape properties creates {@link CheckBox},
     * for other types creates {@link TextField}
     *
     * @param property - property of shape
     * @return - user's control for interacting with property.
     */
    static Control createInputControlProperty(ShapeProperty property) {
        Control control;
        if (property instanceof BooleanShapeProperty) {
            control = new CheckBox();
        } else {
            control = new TextField();
        }
        return control;
    }

    /**
     * Bind {@link ShapeProperty} and {@link Control}: isVisible property and value.
     *
     * @param control control for binding
     * @param prop    ShapeProperty for binding
     */
    static void bindControlWithShapeProperty(Control control, ShapeProperty prop) {
        control.visibleProperty().bindBidirectional(prop.isVisibleProperty());
        if (prop instanceof BooleanShapeProperty && control instanceof CheckBox) {
            ((CheckBox) control).selectedProperty().bindBidirectional((BooleanShapeProperty) prop);
        } else if (control instanceof TextField) {
            ((TextField) control).textProperty().bindBidirectional(prop, prop.getStringConverter());
        } else
            throw new UnsupportedOperationException("Could not bind ShapeProperty with control. Control is " + control + ", property is " + prop);
    }

}
