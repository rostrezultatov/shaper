package ru.ponomarev.shaper;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testfx.api.FxService;
import org.testfx.framework.junit.ApplicationTest;
import ru.ponomarev.shaper.model.Shape;
import ru.ponomarev.shaper.util.property.IntegerShapeProperty;
import ru.ponomarev.shaper.util.property.ShapeProperty;
import ru.ponomarev.shaper.util.validator.ValidationRule;

import java.io.IOException;

import static javafx.application.Platform.runLater;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.testfx.api.FxAssert.verifyThat;

public class ShapeDialogTest extends ApplicationTest {

    private static final Logger log = LoggerFactory.getLogger(ShapeDialogTest.class);

    @Override
    public void start(Stage stage) throws Exception {
        runLater(() -> {
            try {
                String fxmlFile = "/fxml/main.fxml";

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource(fxmlFile));
                Parent rootNode = loader.load(MainApp.class.getResourceAsStream(fxmlFile));

                log.debug("Showing JFX scene");
                Scene scene = new Scene(rootNode, 400, 250);
                scene.getStylesheets().add("/styles/styles.css");

                stage.setTitle("Hello Shaper");
                stage.setScene(scene);
                stage.show();


            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Pause for next UI event
     */
    private void waitEvent() {
        FxService.serviceContext().getBaseRobot().awaitEvents();
    }

    TextField getTextFieldOfProperty(int index) {
        return (TextField) lookup("#propertyGrid > .text-field").selectAt(index).queryFirst();
    }

    private void uiAction(Runnable action) {
        runLater(action);
        waitEvent();
    }

    @Before
    public void waitEventBeforeTest() {
        waitEvent();
    }

    private void EditFirstRow() {
        doubleClickOn("#shapeTable .table-row-cell");
    }

    @After
    public void closeDialog() {
        if (lookup("#propertyGrid").queryFirst() != null && lookup("#propertyGrid").queryFirst().isVisible()) {
            clickOn("#cancelButton");
        }
    }

    @Test
    public void testShouldDisplayDialog() {
        EditFirstRow();
        GridPane grid = (GridPane) lookup("#propertyGrid").tryQueryFirst().orNull();
        verifyThat("#propertyGrid", Node::isVisible);
        verifyThat("#propertyGrid", input -> input == grid);
        verifyThat("Name", input -> input instanceof Label);
    }

    @Test
    public void testEnterX() {
        EditFirstRow();
        TextField tfX = getTextFieldOfProperty(1);
        assertEquals(tfX.getText(), "1");
        clickOn("X").write("6");
        uiAction(() -> tfX.setText("64"));
        assertEquals(tfX.getText(), "64");
    }

    @Test
    public void testValidationName() {
        EditFirstRow();
        TextField nameInput = getTextFieldOfProperty(0);
        uiAction(nameInput::clear);
        assertEquals(nameInput.getText(), "");
        verifyThat("Save", Node::isDisabled);
        uiAction(() -> nameInput.setText("SOME NAME"));
        verifyThat("Save", node -> !node.isDisabled());
    }

    /**
     * Test handling of several errors. First we enter wrong "name", then enter wrong "x"
     * rror message should not change until "name" is corrected,
     * because "name" field added to {@link ru.ponomarev.shaper.util.validator.TextFieldValidator#validationRules}
     * earlier than "x"
     */
    @Test
    public void testValidationSeveralErrors() {
        EditFirstRow();
        TextField nameInput = getTextFieldOfProperty(0);
        TextField xInput = getTextFieldOfProperty(1);
        Label errorLabel = lookup("#errorLabel").queryFirst();
        assertNotNull(errorLabel);
        uiAction(nameInput::clear);
        assertEquals(nameInput.getText(), "");
        verifyThat("Save", Node::isDisabled);
        assertEquals(errorLabel.getText(), "Name is required field");
        uiAction(() -> xInput.setText("wrong X"));
        assertEquals(errorLabel.getText(), "Name is required field");
        uiAction(() -> nameInput.setText("Valid name"));
        assertEquals(errorLabel.getText(), "X must be a number");
        uiAction(() -> xInput.setText("4"));
        verifyThat("Save", node -> !node.isDisabled());
    }

    @Test
    public void testTitle() {
        EditFirstRow();
        Label title = lookup("#titleLabel").queryFirst();
        assertEquals("Oval's properties", title.getText());
    }

    @Test
    public void testCreateOvalWorkflow() {
        TableView<Shape> shapeTable = lookup("#shapeTable").queryFirst();
        int rowCount = shapeTable.getItems().size();
        clickOn("Model").clickOn("Create Oval");
        verifyThat("#propertyGrid", Node::isVisible);
        verifyThat("#Name", input -> "".equals(((TextField) input).getText()));
        verifyThat("#errorLabel", Node::isVisible);
        Label errorLabel = lookup("#errorLabel").queryFirst();

        assertEquals(errorLabel.getText(), "Name is required field");
        clickOn("#Name").write("Test Oval");
        assertEquals(errorLabel.getText(), "Radius must be a positive number");
        verifyThat("#saveButton", Node::isDisabled);

        verifyThat("#RadiusY", input1 -> !input1.isVisible());

        clickOn("#Radius").write("41");
        assertEquals(errorLabel.getText(), "");
        verifyThat("#saveButton", input -> !input.isDisabled());

        // change circle to oval
        clickOn("#isCircle");

        verifyThat("#RadiusY", Node::isVisible);
        verifyThat("#RadiusY", input -> ("41".equals(((TextField) input).getText())));
        verifyThat("#saveButton", input -> !input.isDisabled());

        //clear RadiusY
        clickOn("#RadiusY").type(KeyCode.BACK_SPACE, 2);
        clickOn("#RadiusY").write("0");
        verifyThat("#saveButton", Node::isDisabled);

        //enter RadiusY and save
        clickOn("#RadiusY").write("34");
        verifyThat("#saveButton", input -> !input.isDisabled());
        clickOn("#saveButton");

        assertEquals(rowCount + 1, shapeTable.getItems().size());
    }

    @Test
    public void testChangeErrorMessage() {
        String message1 = "Radius must be a positive number";
        String message2 = "RadiusX must be a positive number";
        ShapeProperty prop = new IntegerShapeProperty("Radius", 1, ValidationRule.POSITIVE_NUMBER);
        prop.labelTextProperty().setValue("RadiusX");
        assertEquals(prop.getErrorMessage(), message2);
    }

    @Test
    public void testRedrawExistingErrorMesssage() {
        clickOn("Model").clickOn("Create Oval");
        Label errorLabel = lookup("#errorLabel").queryFirst();
        clickOn("#Name").write("Test Oval");
        assertEquals(errorLabel.getText(), "Radius must be a positive number");
        clickOn("#isCircle");
        assertEquals(errorLabel.getText(), "RadiusX must be a positive number");
        clickOn("#isCircle");
        clickOn("#Radius").write("7");
        assertEquals(errorLabel.getText(), "");
    }

}
