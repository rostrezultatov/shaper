# Readme

Hello!

This is Shaper - sample JavaFX 8 application build on maven, some ui tests and MVC.

This application is editor of shape's properties with table and dialog.

It is designed to allow programmer ease extending this application:

* adding new shapes with complex properties' interaction and custom validation rules
* grouping shapes by common properties
* adding new views to application

### In Details

#### 1. Adding new shapes

All instatces of `ShapeProperty`, such as "name", "x", "width" etc., are JavaFX's properties.
So their interaction each other can be easily configured by JavaFX bindings.

And this model interaction automatically repeated in UI-interaction, in ShapeDialog. See Oval's behavior.

It is easy to add custom validation rule by inserting new entry in `ValidationRule` enum.

#### 2. Grouping shapes

ShapeDialog gathers all properties of shape from whole class hierarchy, beginning from topmost one.
So shapes can be grouped by introducing common class that extends `model.Shape` class.

All properties in Dialog are displayed in the same order that they are declared in java classes.

#### 3. adding new views

Application designed for easy adding new views into main view.

- `ShapeTable.fxml` included inside `main.fxml` and installed in one block of `BorderPane`
Programmer can create other fxml-files and put them in other `BorderPane`s blocks
- application's model `ShapeList` is made accessible for controllers of new views


### Installation

 1. Check if you have JDK 8 and maven installed.
 2. `mvn package`
 3. `java -jar target/shaper-all.jar`

Best regards,
Pavel

> Written with [StackEdit](https://stackedit.io/).